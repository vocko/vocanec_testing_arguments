
import BaseHTTPServer
from case_handlers import *
import os
import sys
import settings
import posixpath
import urllib
import shutil
import re
try:
    from cStringIO import StringIO
except ImportError:
    from StringIO import StringIO

# TODO:
# - handle errors
#   - 501 for features not yet implemented
# - handle multiple files
# - handle different mime types
# - move handlers to other class
# - create simple config file
# - error log i access log
readOnly = False
class MySimpleServer(BaseHTTPServer.BaseHTTPRequestHandler):
    CASES = [
            CaseSiteRootHandler(),
            CaseFileExistsHandler(),
            CaseFileNotExistsHandler(),
            ]

    # Obradi GET request
    def do_GET(self):
        print "Headers: " + str(self.headers)
        print "Path: ", self.path
        sys.stdout.flush()
        #os.getcwd -> get_current_working_directory
        #self.path -> putanja do filea kojeg browser trazi
        #print os.getcwd() + self.path
        try:
            file_path = self.path
            for case in self.CASES:
                if case.test(file_path):
                    self.send_content(case.run(file_path))
                    break
        except CaseError as e:
            self.handle_error(e.error_code, e.message)


    # Obradi POST request
    def do_POST(self):
        print "Headers: " + str(self.headers)
        print "Path: ", self.path
        sys.stdout.flush()
        r, info = self.deal_post_data()
        print r, info, "by: ", self.client_address
        f = StringIO()
        f.write('<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">')
        f.write("<html>\n<title>Upload Result Page</title>\n")
        f.write("<body>\n<h2>Upload Result Page</h2>\n")
        f.write("<hr>\n")
        if r:
            f.write("<strong>Success:</strong>")
        else:
            f.write("<strong>Failed:</strong>")
        f.write(info)
        f.write("</body>\n</html>\n")
        length = f.tell()
        f.seek(0)
        self.send_response(200)
        self.send_header("Content-type", "text/html")
        self.send_header("Content-Length", str(length))
        self.end_headers()
        if f:
            self.copyfile(f, self.wfile)
            f.close()


    def deal_post_data(self):
        print self.headers
        boundary = self.headers.plisttext.split("=")[1]
        print 'Boundary %s' %boundary
        remainbytes = int(self.headers['content-length'])
        print "Remain Bytes %s" %remainbytes
        line = self.rfile.readline()
        remainbytes -= len(line)
        if not boundary in line:
            return (False, "Content NOT beginning with boundary")
        line = self.rfile.readline()
        remainbytes -= len(line)
        fn = re.findall(r'Content-Disposition.*name="file"; filename="(.*)"', line)
        if not fn:
            return (False, "Can't find out file name...")
        path = self.translate_path(self.path)
        fn = os.path.join(path, fn[0])
        line = self.rfile.readline()
        remainbytes -= len(line)
        line = self.rfile.readline()
        remainbytes -= len(line)
        try:
            out = open(fn, 'wb')
        except IOError:
            return (False, "Can't create file to write, do you have permission to write?")

        if line.strip():
            preline = line
        else:
            preline = self.rfile.readline()
        remainbytes -= len(preline)
        while remainbytes > 0:
            line = self.rfile.readline()
            remainbytes -= len(line)
            if boundary in line:
                preline = preline[0:-1]
                if preline.endswith('\r'):
                    preline = preline[0:-1]
                out.write(preline)
                out.close()
                return (True, "File '%s' upload success!" % fn)
            else:
                out.write(preline)
                preline = line
        return (False, "Unexpected End of data.")


    def translate_path(self, path):
        """Translate a /-separated PATH to the local filename syntax.

        Components that mean special things to the local file system
        (e.g. drive or directory names) are ignored.

        """
        # abandon query parameters
        path = path.split('?',1)[0]
        path = path.split('#',1)[0]
        path = posixpath.normpath(urllib.unquote(path))
        words = path.split('/')
        words = filter(None, words)
        path = os.getcwd()
        for word in words:
            drive, word = os.path.splitdrive(word)
            head, word = os.path.split(word)
            if word in (os.curdir, os.pardir): continue
            path = os.path.join(path, word)
        return path


    def copyfile(self, source, outputfile):
        """Copy all data between two file objects.

        The SOURCE argument is a file object open for reading
        (or anything with a read() method) and the DESTINATION
        argument is a file object open for writing (or
        anything with a write() method).

        """
        shutil.copyfileobj(source, outputfile)


    def full_file_path(self):
        return os.path.join(os.getcwd(), "www", self.path[1:])

    def handle_error(self, code, msg):
        self.send_response(code)
        self.send_header("Content-Type", "text/html")
        if code != 200:
            page = utils.get_asset_content(code, "www")
        else:
            page = "<html><body><p>"+str(msg)+"</p></body></html>"
        self.send_header("Content-Lenght", str(len(page)))
        self.end_headers()
        self.wfile.write(page)


    def send_content(self, content):
        if(readOnly):
            print "Server is in Read-Only mode, cannot send files"
            return
        self.send_response(200)
        self.send_header("Content-Type", "text/html")
        self.send_header("Content-Lenght", str(len(content)))
        self.send_header("Cache-Control", "no-cache")
        self.end_headers()
        self.wfile.write(content)


def main():
    readOnly = False
    serverPort = 8080

    print "Starting web server..."

    # get full path to shared directory
    if len(sys.argv) < 2:
        print "Add path to shared directory as argument"
        sys.exit(1)
    else:
        share_root = utils.get_share_full_path(sys.argv[1])
    
    [serverPort, readOnly] = utils.parse_args(sys.argv)
    print "ServerPort: %d; readOnly: %s"%(serverPort, readOnly)

    ##print len(sys.argv), readOnly, serverPort

    # Initialize settings globals with full path to share root
    settings.init(share_root)
    
    settings.options['serverPort'] = serverPort
    settings.options['readOnly'] = readOnly

    serverAddress = ('', serverPort)
    server = BaseHTTPServer.HTTPServer(
                                serverAddress, MySimpleServer)
    server.serve_forever()

if __name__ == '__main__':
    main()



