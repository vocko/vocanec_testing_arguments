import pytest
import inspect, os, sys
from simplefss import utils
from simplefss import settings

def test_vocanec():
	arg = "simplefss .".split(' ')
	assert utils.parse_args(arg) == (8080, False)

def test_vocanec_port():
	arg = "simplefss . --port".split(' ')
	assert utils.parse_args(arg) == (8080, False)

def test_vocanec_port_1122():
	arg = "simplefss . --port 1122".split(' ')
	assert utils.parse_args(arg) == (1234, False)

def test_vocanec_read_only():
	arg = "simplefss . --readonly".split(' ')
	assert utils.parse_args(arg) == (8080, True)

def test_read_only_port():
	arg = "simplefss . --readonly --port".split(' ')
	assert utils.parse_args(arg) == (8080, True)

def test_read_only_port_99():
	arg = "simplefss . --readonly --port 99".split(' ')
	assert utils.parse_args(arg) == (1, True)
